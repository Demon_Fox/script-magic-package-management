//License
//
//Neither the distributor or writer is responsible for anything.

//This file requires string.h

// arg is the variable being searched for in config.
// the position in config where arg is preset is returned

// start of upper case range in ascii
#define UPPERCASE_START  65
// End of upper case range in ascii
#define UPPERCASE_END    90
// Start of lower case range in ascii
#define LOWERCASE_START  97
// End of lower case range in ascii
#define LOWERCASE_END   122

//returns FALSE if invalid, and returns TRUE if valid
int
linevalid(char *in, char *arg)
{
    int i,c;

    //Checks to see if line is sane
    for (i = 0;;i++)
    {
        c = in[i];
        if      (c == ' ')
            continue;
        else if (c >= UPPERCASE_START && c <= UPPERCASE_END)
            continue;
        else if (c >= LOWERCASE_START && c <= LOWERCASE_END)
            continue;
        else if (c == '=')
            continue;
        else if (c == '\0' || c == '\n')
            return FALSE;
        else
            return FALSE;
    }

    for (i = 0;;i++)
    {
        c = in[i];
        if (c == ' ')
            //Something goes here
    
    }
    //Not finished
}


char*
config_line(char *config, char *arg)
{
    //clen = config length
    //alen = arg length
    int alen,clen;
    alen = strlen(   arg);
    clen = strlen(config);

    if      (alen < 1)
        return NULL;
    else if (clen < 1)
        return NULL;

    //return variable
    char* ret;
    ret = NULL;

    int i,j;
    //tmp char
    char c;

    int tmp;
    for (i = 0; i < clen; i+=tmp)
    {
        c = config[i];

        switch (c)
        {
            //Spaces get ignored
            case ' ':
                tmp = 1;
                break;
            //comments get ignored
            case '#':
                for (j = 0; i+j < clen; j++)
                {
                    tmp = config[i+j];
                    if      (tmp == NULL)
                        return ret;
                    else if (tmp == '\n')
                    {
                        tmp = j;
                        break;
                    }
                }
                break;
            //Hit end of config and found NO'SING!
            case '\0':
                return ret;
                break;
            //Line breaks get ignored
            case '\n':
                tmp = 1;
                break;
            //This is where the magic happens
            default:
                //line validation
                tmp = linevalid(config+i+j,arg);
        }
    }
    //Not finished

    //Code goes here.

    //Anything that slips through the cracks returns NULL
    return NULL;
}





