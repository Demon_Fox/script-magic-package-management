//License:
//The writer/distributor of this code is not responsible for anything.

struct  {
	int pos;
}args;

int init_arg(argc)  {
	args.pos = 0;
}

char* readarg(int argc, char **argv)
{
	if (argc == 1)  {
		return(0);
	}

	if (argc > args.pos)  {
		args.pos++;
		return((char*) argv[args.pos]);
	}

	return(NULL);
}
char* rewindarg(int argc, char **argv)
{
	if (argv == 1)  {
		return(0);
	}
	if (args.pos > 1)  {
		args.pos--;
		return((char*) argv[args.pos]);
	}

	return(NULL);
}


/* This could be used for something later
char* readargarg(int argc, char **argv)
{
	if (argc == 1)  {
		return(0);
	}

	if (argc > args.pos)  {
		args.pos++;
		return((char*) argv[args.pos]);
	}

	return(NULL);
}
*/
